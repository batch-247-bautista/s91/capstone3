import React, { useState } from 'react';
import { Link } from 'react-router-dom';

function Admin() {
  const [loggedIn, setLoggedIn] = useState(false); // state for tracking login status
  const [dropdownOpen, setDropdownOpen] = useState(false); // state for tracking dropdown menu status

  const handleLogin = () => {
    setLoggedIn(true);
  };

  const handleLogout = () => {
    setLoggedIn(false);
  };

  const handleDropdownToggle = () => {
    setDropdownOpen(!dropdownOpen);
  };

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container">
        <Link className="navbar-brand" to="/">
          My Website
        </Link>

        {/* Login/logout button */}
        <button
          className="navbar-toggler"
          type="button"
          onClick={loggedIn ? handleLogout : handleLogin}
        >
          {loggedIn ? 'Logout' : 'Login'}
        </button>

        {/* Dropdown menu */}
        {loggedIn && (
          <div className="dropdown">
            <button
              className="btn btn-secondary dropdown-toggle"
              type="button"
              onClick={handleDropdownToggle}
            >
              Admin
            </button>
            <div className={`dropdown-menu${dropdownOpen ? ' show' : ''}`}>
              <Link className="dropdown-item" to="/add-product">
                Add Product
              </Link>
              <Link className="dropdown-item" to="/archive-product">
                Archive Product
              </Link>
              <Link className="dropdown-item" to="/inventory">
                Inventory
              </Link>
              <Link className="dropdown-item" to="/user-settings">
                User Settings
              </Link>
            </div>
          </div>
        )}
      </div>
    </nav>
  );
}

export default Admin;
