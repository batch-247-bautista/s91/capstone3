import React, { useState, useEffect } from 'react';

const UpdateProductDetails = ({ match }) => {
  const [product, setProduct] = useState(null);

  useEffect(() => {
    const fetchProduct = async () => {
      const response = await fetch(`https://capstone-00002.onrender.com/products/${match.params.id}`);
      const data = await response.json();
      setProduct(data);
    };
    fetchProduct();
  }, [match.params.id]);

  const handleUpdate = (productId, name, description, price) => {
    const updateEndpoint = `https://capstone-00002.onrender.com/products/${productId}/update`;
    const updates = { name, description, price };
    fetch(updateEndpoint, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(updates),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        // redirect the user back to the inventory page
        window.location.href = '/inventory';
      })
      .catch((err) => console.error(err));
  };

  if (!product) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h1>Update Product</h1>
      <form onSubmit={(event) => {
        event.preventDefault();
        handleUpdate(product._id, event.target.name.value, event.target.description.value, event.target.price.value);
      }}>
        <div>
          <label htmlFor="name">Name</label>
          <input type="text" id="name" defaultValue={product.name} required />
        </div>
        <div>
          <label htmlFor="description">Description</label>
          <textarea id="description" defaultValue={product.description} required />
        </div>
        <div>
          <label htmlFor="price">Price</label>
          <input type="number" id="price" defaultValue={product.price} required />
        </div>
        <button type="submit">Update Product</button>
      </form>
    </div>
  );
};

export default UpdateProductDetails;
